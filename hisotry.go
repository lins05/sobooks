package main

import (
	"log"
	"net/http"
	"time"
)

var _ = log.Println
var _ = time.Now

// Cookie name for store user search history
const COOKIE_NAME = "hid"

// Cookie max age, 1 year
const COOKIE_MAX_AGE = 3600 * 24 * 365

// String used to seperate search history
const HISTORY_SEP = "\000"

// Max number of search history stored in server per user
var MAX_SEARCH_HISTORY = 5

// Update the search history of this user in database, and returns an updated
// list of recent search history
func updateSearchHistory(w http.ResponseWriter, r *http.Request, keyword string) []string {
	var (
		id      string
		history []string
	)

	c, err := r.Cookie(COOKIE_NAME)
	if err != nil {
		// No cookie yet, set a new one
		id, _ = genUuid()
		c = &http.Cookie{
			Name:    COOKIE_NAME,
			Value:   id,
			Path:    "/",
			Expires: time.Now().Add(time.Second * time.Duration(COOKIE_MAX_AGE)),
		}

		http.SetCookie(w, c)

	} else {
		// Has cookie: update search history
		id = c.Value
		if len(id) != 36 {
			// Not a valid uuid
			return nil
		}
		history = readSearchHistory(id)
	}

	newHistory := calcSearchHistory(history, keyword)
	storeSearchHistory(id, newHistory)

	return newHistory
}

func calcSearchHistory(history []string, keyword string) []string {
	var newHistory []string
	l := len(history)
	if l == 0 {
		newHistory = []string{keyword}
	} else {
		pos := -1
		for i, h := range history {
			if h == keyword {
				pos = i
				break
			}
		}

		if pos < 0 {
			newHistory = append([]string{keyword}, history...)
		} else if pos == 0 {
			newHistory = history
		} else if pos == l-1 {
			newHistory = append([]string{keyword}, history[:l-1]...)
		} else if pos > 0 {
			newHistory = append([]string{keyword}, history[:pos]...)
			newHistory = append(newHistory, history[pos+1:]...)
		}
	}

	if len(newHistory) > MAX_SEARCH_HISTORY {
		newHistory = newHistory[:MAX_SEARCH_HISTORY]
	}

	return newHistory
}
