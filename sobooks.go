package main

import (
	// "fmt"
	"log"
	"net/http"
)

// Re-compile template for every request. 
var DEBUG_TEMPLATE = false

// Fetch search url from proxy
var USE_PROXY = false

// Use GAE proxy or local GAE sdk. Only valid if USE_PROXY is "true".
var USE_GAE = true

// Send a GET request if GAE proxy is idle for too long
var ENABLE_GAE_KEEPALIVE = true

func init_log() {
	log.SetFlags(log.LstdFlags)
}

func print_start_info() {
	log.Println("Site root:     \t", SETTINGS["SITE_ROOT"])
	log.Println("Static root:   \t", SETTINGS["STATIC_ROOT"])
	log.Println("Debug template:\t", DEBUG_TEMPLATE)
	log.Println("Use PROXY:     \t", USE_PROXY)
	if USE_PROXY {
		log.Println("Use GAE:       \t", USE_GAE)
		log.Println("Proxies:")
		if USE_GAE {
			for _, u := range GAE_PROXY_URLS {
				log.Printf("                 \t%s\n", u)
			}
		} else {
			for _, u := range LOCAL_PROXY_URLS {
				log.Printf("\t%s\n", u)
			}
		}
	}
}

func main() {
	init_log()

	host := "127.0.0.1:9980"

	print_start_info()
	log.Printf("start to listen on %s\n", host)

	http.HandleFunc("/", homePageHanlder)
	http.HandleFunc("/search", searchPageHandler)
	http.HandleFunc("/asearch", ajaxSearchHandler)
	http.HandleFunc("/feedback", feedbackHandler)
	http.HandleFunc("/feedbacklist", listFeedbackHandler)
	http.HandleFunc("/bestseller", bestSellerHandler)

	log.Fatal(http.ListenAndServe(host, nil))
}
