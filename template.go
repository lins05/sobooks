package main

import (
	"fmt"
	"html/template"
	"path/filepath"
	"reflect"
	"strconv"
	"strings"
	"sync"
	"unicode/utf8"
)

var _ = fmt.Println

var cachedTemplates = map[string]*template.Template{}
var cachedMutex sync.Mutex

var templateFuncs = template.FuncMap{
	"eq":   eq,
	"neq":  neq,
	"g":    getSettings,
	"trim": trim,
}

func T(name string) *template.Template {
	if !DEBUG_TEMPLATE {
		// Turn off templates cache when debug 
		cachedMutex.Lock()
		defer cachedMutex.Unlock()

		if t, ok := cachedTemplates[name]; ok {
			return t
		}
	}

	t := template.New("base.html").Funcs(templateFuncs)

	t = template.Must(t.ParseFiles(
		"templates/base.html",
		filepath.Join("templates", name),
	))

	return t
}

func T0(name string) *template.Template {
	if !DEBUG_TEMPLATE {
		// Turn off templates cache when debug 
		cachedMutex.Lock()
		defer cachedMutex.Unlock()

		if t, ok := cachedTemplates[name]; ok {
			return t
		}
	}

	fullpath := filepath.Join("templates", name)
	t := template.New(name).Funcs(templateFuncs)
	t = template.Must(t.ParseFiles(fullpath))

	cachedTemplates[name] = t

	return t
}

func eq(args ...interface{}) bool {
	if len(args) == 0 {
		return false
	}
	x := args[0]
	switch x := x.(type) {
	case string, int, int64, byte, float32, float64:
		for _, y := range args[1:] {
			if x == y {
				return true
			}
		}
		return false
	}

	for _, y := range args[1:] {
		if reflect.DeepEqual(x, y) {
			return true
		}
	}
	return false
}

func neq(args ...interface{}) bool {
	return !eq(args...)
}

func convertStars(i interface{}) []byte {
	s, ok := i.(string)
	if !ok {
		return nil
	}
	n, err := strconv.Atoi(s)
	if err != nil {
		return nil
	}
	if n > 5 {
		n = 5
	} else if n < 0 {
		n = 0
	}
	return make([]byte, n)
}

type TextSegment struct {
	Text   string
	IsLink bool
}

func convertAuthorLinks(ifaceA interface{}, ifaceL interface{}) []*TextSegment {
	text, ok := ifaceA.(string)
	if !ok || len(text) == 0 {
		return nil
	}
	links_str, ok := ifaceL.(string)
	if !ok || len(links_str) == 0 {
		return nil
	}

	links := strings.Split(links_str, "\t")
	nLinks := len(links)

	segments := make([]*TextSegment, 0, 2*nLinks)
	for i, link := range links {
		pos := strings.Index(text, link)
		if pos != -1 {
			if pos != 0 {
				pre := text[0:pos]
				segments = append(segments, &TextSegment{pre, false})
			}

			segments = append(segments, &TextSegment{link, true})
			offset := pos + len(link)
			if len(text) > offset {
				if i == nLinks-1 {
					segments = append(segments, &TextSegment{text[offset:], false})
					break
				} else {
					text = text[offset:]
					continue
				}
			} else {
				break
			}

		} else {
			// impossible
			break
		}
	}

	return segments
}

func convertAuthorLinksAmazon(i interface{}) []*TextSegment {
	s, ok := i.(string)
	if !ok {
		return nil
	}

	authors := strings.Split(s, "、")
	segments := make([]*TextSegment, 0, len(authors))
	for _, name := range authors {
		name = strings.TrimSpace(name)
		if name == "等" {
			segments = append(segments, &TextSegment{name, false})
		} else {
			segments = append(segments, &TextSegment{name, true})
		}
	}

	return segments
}

func trim(i interface{}, m interface{}) interface{} {
	var (
		max int
		err error
	)

	switch t := m.(type) {
	case string:
		max, err = strconv.Atoi(t)
		if err != nil {
			return i
		}
	case int:
		max = t
	}

	switch s := i.(type) {
	case string:
		if len(s) > max {
			s = s[:max]
			for !utf8.ValidString(s) && len(s) > 1 {
				s = s[:len(s)-1]
			}
			s += ".."
		}
		return s
	}

	return i
}
