package main

import (
	"github.com/ziutek/mymysql/autorc"
	"github.com/ziutek/mymysql/mysql"
	_ "github.com/ziutek/mymysql/thrsafe"
	"log"
	"strings"
	"time"
)

// This file handles database initialization and provides two type of methods:
// storeXXXX : write to databse
// readXXX: read from database

var db *autorc.Conn

func init() {
	user := "sobooks"
	pass := "sobooks"
	dbname := "sobooks"
	proto := "tcp"
	addr := "127.0.0.1:3306"

	db = autorc.New(proto, "", addr, user, pass, dbname)
	db.Register("set names utf8")

	var (
		sql string
		err error
	)
	sql = "CREATE TABLE IF NOT EXISTS FeedBack (email VARCHAR(256), suggestion TEXT NOT NULL, timestamp DATETIME NOT NULL)"
	_, _, err = db.Query(sql)
	if err != nil {
		log.Fatalln("Failed to create feedback table:", err)
	}

	sql = "CREATE TABLE IF NOT EXISTS SearchHistory (id VARCHAR(36) PRIMARY KEY, hist TEXT NOT NULL, timestamp DATETIME NOT NULL)"
	_, _, err = db.Query(sql)
	if err != nil {
		log.Fatalln("Failed to create history table:", err)
	}

	sql = "CREATE TABLE IF NOT EXISTS BestSeller (site VARCHAR(36) PRIMARY KEY, content TEXT NOT NULL, timestamp DATETIME NOT NULL)"
	_, _, err = db.Query(sql)
	if err != nil {
		log.Fatalln("Failed to create bestseller table:", err)
	}
}

func recordFeedback(email, suggestion string) {
	now := mysql.TimeString(time.Now())
	_, _, err := db.Query("INSERT INTO FeedBack Values('%s', '%s', '%s')", email, suggestion, now)
	if err != nil {
		log.Println(err)
	}
}

type Feedback struct {
	Email, Suggestion string
	Timestamp         time.Time
}

func getFeedback() ([]*Feedback, error) {
	rows, _, err := db.Query("SELECT * from FeedBack ORDER BY timestamp DESC")
	if err != nil {
		log.Println(err)
		return nil, err
	}

	var fbs []*Feedback
	for _, row := range rows {

		fb := &Feedback{
			Email:      row.Str(0),
			Suggestion: row.Str(1),
			Timestamp:  row.ForceLocaltime(2),
		}
		fbs = append(fbs, fb)
	}

	return fbs, nil
}

func readSearchHistory(id string) []string {
	sql := "SELECT hist From SearchHistory WHERE id = '%s'"
	rows, _, err := db.Query(sql, id)
	if err != nil {
		log.Println("Error when get history:", err)
		return nil
	}

	var val string
	for _, row := range rows {
		val = row.Str(0)
	}

	if val == "" {
		return nil
	}

	return strings.Split(val, HISTORY_SEP)
}

func storeSearchHistory(id string, history []string) {
	var (
		sql, hist string
		err       error
	)

	now := mysql.TimeString(time.Now())
	if len(history) > 1 {
		hist = strings.Join(history, HISTORY_SEP)
	} else {
		hist = history[0]
	}

	if len(readSearchHistory(id)) != 0 {
		sql = "UPDATE SearchHistory SET hist = '%s', timestamp = '%s'  WHERE id = '%s'"
		_, _, err = db.Query(sql, hist, now, id)
	} else {
		sql = "INSERT INTO SearchHistory VALUES('%s', '%s', '%s')"
		_, _, err = db.Query(sql, id, hist, now)
	}

	if err != nil {
		log.Println("Error when set history:", err)
	}
}

func storeBestSeller(site string, content []byte) {
	sql := "REPLACE INTO BestSeller VALUES(?, ?, ?)"
	stmt, err := db.Prepare(sql)
	if err != nil {
		log.Println("Error when save BestSeller :", err)
		return
	}

	_, _, err = stmt.Exec(site, content, time.Now())
	if err != nil {
		log.Println("Error when save BestSeller :", err)
	}
}

func readBestSeller(site string) ([]byte, error) {
	sql := "SELECT content FROM BestSeller where site = '%s'"
	rows, _, err := db.Query(sql, site)
	if err != nil {
		log.Println("Error when save BestSeller :", err)
		return nil, err
	}

	if len(rows) == 0 {
		return nil, nil
	}

	return rows[0].Bin(0), nil
}
