package main

const ccnet_port = 63419

var SETTINGS = map[string]string{
	"SITE_ROOT":   "/sobooks/",
	"STATIC_ROOT": "/sobooks/static/",
}

func getSettings(key string) string {
	value, ok := SETTINGS[key]
	if ok {
		return value
	}

	return ""
}
