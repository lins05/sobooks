package main

import (
	"fmt"
	"log"
	"net/http"
	"net/url"
	"strings"
	"time"
)

var _ = fmt.Println

var GAE_PROXY_URLS = []string{
	"http://linshuaigae01.appspot.com",
	"http://linshuaigae02.appspot.com",
	"http://linshuaigae03.appspot.com",
}

var LOCAL_PROXY_URLS = []string{
	"http://127.0.0.1:8080/urlfetch",
}

var proxyChan chan string

var gaeLastVisited []time.Time

const KEEPALIVE_CHECK_INTERVAL = 5 // Every 5 senconds
const GAE_MAX_IDLE_SECONDS = 60    // 1 minutes

func sendGet(url string) {
	resp, err := http.Get(url)
	if err != nil {
		log.Printf("Error When send GET to %s: %s\n", url, err)
		return
	}
	resp.Body.Close()
}

// Google app engine would shutdown an application if it's idle for ~5
// minutes. To avoid this, we send a GET request every 4 minutes to make
// proxies online.
func gaeKeepAlive() {
	log.Println("GAE Keep alive started")
	interval := time.Second * time.Duration(KEEPALIVE_CHECK_INTERVAL)
	gaeLastVisited = make([]time.Time, len(GAE_PROXY_URLS))
	for {
		now := time.Now()
		for i, lastVisit := range gaeLastVisited {
			if now.Unix()-lastVisit.Unix() > GAE_MAX_IDLE_SECONDS {
				u := GAE_PROXY_URLS[i]
				log.Println("Send GET to", u)
				gaeLastVisited[i] = now

				go sendGet(u)
			}
		}
		<-time.After(interval)
	}
}

func init() {
	go func() {
		if USE_PROXY {
			proxyChan = make(chan string)
			proxies := LOCAL_PROXY_URLS
			if USE_GAE {
				proxies = GAE_PROXY_URLS
				if ENABLE_GAE_KEEPALIVE {
					go gaeKeepAlive()
				}
			}
			totalProxies := len(proxies)

			i := 0
			for {
				proxyChan <- proxies[i]
				gaeLastVisited[i] = time.Now()
				i++
				if i == totalProxies {
					i = 0
				}
			}
		}
	}()
}

type SortParamTable map[string]map[string]string

var sortParamTable = SortParamTable{
	"dangdang": {
		"sales":   "sort_type=sort_xsale_desc",
		"price":   "sort_type=sort_saleprice_asc",
		"-price":  "sort_type=sort_saleprice_desc",
		"reviews": "sort_type=sort_total_review_count_desc",
		"pubdate": "sort_type=sort_pubdate_desc",
	},
	"360buy": {
		"sales":   "rt=1&psort=3",
		"price":   "rt=1&psort=2",
		"-price":  "rt=1&psort=1",
		"reviews": "rt=1&psort=4",
		"pubdate": "rt=1&psort=6",
	},
	"amazon": {
		"sales":   "sort=salesrank",
		"price":   "sort=price",
		"-price":  "sort=-price",
		"reviews": "sort=reviewrank_authority",
		"pubdate": "sort=-pubdate",
	},
}

func isValidSort(site, sort string) bool {
	dict, ok := sortParamTable[site]
	if !ok {
		return false
	}
	_, ok = dict[sort]
	if !ok {
		return false
	}

	return true
}

func getSortParam(site, sort string) string {
	table := sortParamTable[site]

	return "&" + table[sort]
}

func getSearchUrl(site, keyword, sort string) (searchUrl string, err error) {
	switch site {
	case "dangdang":
		gbk, err := utf8ToGbk(keyword)
		if err != nil {
			return "", err
		}
		searchUrl = "http://searchb.dangdang.com/?key="
		searchUrl += url.QueryEscape(gbk)
		searchUrl += "&category_path=01.00.00.00.00.00"
	case "360buy":
		quoted := url.QueryEscape(keyword)
		searchUrl = "http://search.360buy.com/Search?enc=utf-8&book=y&keyword="
		searchUrl += quoted
	case "amazon":
		searchUrl = "http://www.amazon.cn/s/ref=nb_sb_noss_1?__mk_zh_CN=%E4%BA%9A%E9%A9%AC%E9%80%8A%E7%BD%91%E7%AB%99&url=search-alias%3Dstripbooks&field-keywords="

		searchUrl += url.QueryEscape(keyword)
		searchUrl += "&x=0&y=0"
	}

	if sort != "default" {
		searchUrl += getSortParam(site, sort)
	}

	return searchUrl, nil
}

func getProxyUrl(searchUrl string) string {
	proxy := <-proxyChan
	proxy += "/urlfetch" + "?maxRetry=3&deadline=7" + "&url=" + url.QueryEscape(searchUrl)

	return proxy
}

type searchResult struct {
	books                    []map[string]interface{}
	totalPages               int
	prevPageUrl, nextPageUrl string
}

// If the ajax search is for a "prev-page" or "next-page" page request, then the url
// would be provided in the ajax params. If the search is for a first search,
// then the url should be constructed from the (site, keyword, sort)  args.
func getBookInfo(site, url, keyword, sort string) (ret *searchResult, err error) {
	var searchUrl, u string
	if url != "" {
		if !strings.HasPrefix(url, "http") {
			var prefix string
			switch site {
			case "dangdang":
				prefix = "http://searchb.dangdang.com"
			case "360buy":
				prefix = "http://search.360buy.com"
			case "amazon":
				prefix = "http://www.amazon.cn"
			}

			if url[0] != '/' {
				url = "/" + url
			}

			url = prefix + url
		}

		searchUrl = url

	} else {
		searchUrl, err = getSearchUrl(site, keyword, sort)
		if err != nil {
			return
		}
	}

	if USE_PROXY {
		u = getProxyUrl(searchUrl)
	} else {
		u = searchUrl
	}

	buf, err := getPage(u, 3)
	if err != nil {
		return
	}

	ret, err = parsePage(site, buf)
	return ret, err
}
