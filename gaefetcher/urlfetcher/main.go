package urlfetcher

import (
	"appengine"
	"appengine/urlfetch"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"
)

func init() {
	http.HandleFunc("/urlfetch", fetchUrl)
}

const DefaultFetchDeadlineSeconds = 7
const DefaultMaxRetry = 2

func fetchUrl(w http.ResponseWriter, r *http.Request) {
	var err error
	var maxRetry, deadline int

	url := r.FormValue("url")
	deadlineStr := r.FormValue("deadline")
	maxRetryStr := r.FormValue("maxRetry")

	c := appengine.NewContext(r)

	if len(url) == 0 {
		http.Error(w, "bad url", http.StatusBadRequest)
		return
	}

	if len(deadlineStr) == 0 {
		deadline = DefaultFetchDeadlineSeconds
	} else {
		deadline, err = strconv.Atoi(deadlineStr)
		if err != nil || deadline < 2 || deadline > 10 {
			http.Error(w, "bad deadline", http.StatusBadRequest)
			return
		}
	}

	if len(maxRetryStr) == 0 {
		maxRetry = DefaultMaxRetry
	} else {
		maxRetry, err = strconv.Atoi(maxRetryStr)
		if err != nil || maxRetry < 1 || maxRetry > 3 {
			http.Error(w, "bad maxRetry", http.StatusBadRequest)
			return
		}
	}

	// client := urlfetch.Client(c)
	client := &http.Client{
		Transport: &urlfetch.Transport{
			Context:  c,
			Deadline: time.Second * time.Duration(deadline),
		},
	}

	for retried := 0; retried < maxRetry; retried++ {
		resp, err := client.Get(url)
		if err != nil {
			c.Errorf("error when invoking urlfetch: %s\n", err)
			continue
		}
		if resp.StatusCode != 200 {
			c.Errorf("error when invoking urlfetch: bad status code %s\n", resp.StatusCode)
			continue
		}
		defer resp.Body.Close()

		buf, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			c.Errorf("error when reading urlfetch result: %s\n", err)
			continue
		}

		w.Write(buf)
		return
	}

	http.Error(w, err.Error(), http.StatusInternalServerError)
}
