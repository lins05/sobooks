#!/bin/bash

binary=sobooks
set -e

echo "Compiling $binary..."
go build
echo "Starting $binary..."
exec ./${binary}
