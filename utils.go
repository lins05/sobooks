package main

import (
	"fmt"
	"iconv"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
)

// Generate a uuid.
func genUuid() (string, error) {
	f, err := os.Open("/dev/urandom")
	if err != nil {
		return "", nil
	}
	b := make([]byte, 16)
	if _, err := f.Read(b); err != nil {
		return "", nil
	}

	if err := f.Close(); err != nil {
		return "", nil
	}
	uuid := fmt.Sprintf("%x-%x-%x-%x-%x", b[0:4], b[4:6], b[6:8], b[8:10], b[10:])
	return uuid, nil
}

func utf8ToGbk(utf8 string) (gbk string, err error) {
	conv, err := iconv.Open("gbk", "utf-8")
	if err != nil {
		return "", err
	}
	defer conv.Close()

	gbk, err = conv.Conv(utf8)
	if err != nil {
		return "", err
	}

	return gbk, nil
}

func gbkToUtf8(gbk string) (utf8 string, err error) {
	conv, err := iconv.Open("utf-8", "gb18030")
	if err != nil {
		return "", err
	}
	defer conv.Close()

	utf8, err = conv.Conv(gbk)
	if err != nil {
		return "", err
	}

	return utf8, nil
}

func gbkToUtf8Bytes(gbk []byte) (utf8 []byte, err error) {
	conv, err := iconv.Open("utf-8", "gb18030")
	if err != nil {
		return nil, err
	}
	defer conv.Close()

	utf8, err = conv.ConvBytes(gbk)
	if err != nil {
		return nil, err
	}

	return utf8, nil
}

func chop_query(s string) string {
	pos := strings.Index(s, "?")
	if pos < 0 {
		return s
	}

	return s[:pos]
}

func getPage(u string, maxRetry int) (buf []byte, err error) {
	if maxRetry < 1 && maxRetry > 10 {
		maxRetry = 3
	}

	for i := 0; i < maxRetry; i++ {
		buf, err = _getPage(u)
		if err != nil {
			return
		}
	}

	return
}

func _getPage(u string) (buf []byte, err error) {
	r, err := http.Get(u)
	if err != nil {
		return
	}
	if r.StatusCode != 200 {
		err = fmt.Errorf("bad status code %d", r.StatusCode)
		return
	}
	defer r.Body.Close()

	buf, err = ioutil.ReadAll(r.Body)
	if err != nil {
		return
	}

	return
}
