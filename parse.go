package main

import (
	"bytes"
	"exp/html"
	"exp/html/atom"
	"fmt"
	"goquery"
	"math"
	"regexp"
	"strconv"
	"strings"
)

type parser struct {
	encoding       string
	listSelector   string
	itemParseFunc  func(int, *goquery.Selection) map[string]interface{}
	getPrevNextUrl func(*goquery.Document) (int, string, string)
}

// Regexps used to extract specific text from web page
var digitRe = regexp.MustCompile(`[\d]+`)
var dateRe = regexp.MustCompile(`\d{1,4}-\d\d`)
var priceRe = regexp.MustCompile(`[\d]+\.[\d]+`)

var dangdangPubRe = regexp.MustCompile(`(?:(?P<author>.+?)(?:主编|编|著|编著)?/)?(?P<date>\d{1,4}-\d\d-\d\d)/(?P<press>.*)`)
var dangdangTotalPagesRe = regexp.MustCompile(`共([\d]+)页`)

var dangdangBSAuthorRe = regexp.MustCompile(`作者.(.*)`)
var dangdangBSPressRe = regexp.MustCompile(`出版社.(.*)`)

var jingdongPinfoRe = regexp.MustCompile(`：(?P<author>[\s\S]*)[出版社\s]*：(?P<press>.*)`)
var jingdongBSPinfoRe = regexp.MustCompile(`：(?P<author>[\s\S]*)出版社：(?P<press>.*)`)
var jingdongPubdateRe = regexp.MustCompile(`[\d]+年[\d]+月`)
var jingdongTotalPagesRe = regexp.MustCompile(`/([\d]+)`)

var amazon_B_publisherRe = regexp.MustCompile(`(?P<author>.*?)(?P<press>[\S]+出版社) \((?P<pubdate>.*)出版\)`)

var parserTable = map[string]*parser{
	"dangdang": &parser{
		"gb18030",
		"div.publish_main div.resultlist > ul > li",
		parseDangDangItem,
		getPrevNextUrlDangDang,
	},
	"360buy": &parser{
		"gb18030",
		"div.right-extra div#plist div.item",
		parse360buyItem,
		getPrevNextUrl360buy,
	},
	"amazon": &parser{
		"utf-8",
		"div[id^=result_]",
		parseAmazonItem,
		getPrevNextUrlAmazon,
	},
	"dangdang-bs": &parser{
		"gb18030",
		"div.right_list div.book_list",
		parseDangDangBS,
		nil,
	},
	"360buy-bs": &parser{
		"gb18030",
		"div#plist div.item",
		parse360buyBS,
		nil,
	},
	"amazon-bs": &parser{
		"utf-8",
		"ol.zg_list > li",
		parseAmazonBS,
		nil,
	},
}

// Utitily functions
func getDoc(page []byte, encoding string) (*goquery.Document, error) {
	var err error
	if encoding != "utf-8" {
		page, err = gbkToUtf8Bytes(page)
		if err != nil {
			return nil, err
		}
	}
	r := bytes.NewBuffer(page)

	rootNode, err := html.Parse(r)
	if err != nil {
		return nil, err
	}

	return goquery.NewDocumentFromNode(rootNode), nil
}

func getFirstText(s *goquery.Selection, selector string) string {
	r := s.Find(selector)
	if r.Length() > 0 {
		return r.First().Text()
	}

	return ""
}

func calcDiscount(market_price, store_price string) (discount string) {
	mf, err := strconv.ParseFloat(market_price, 32)
	if err != nil || mf == 0 {
		return
	}

	sf, err := strconv.ParseFloat(store_price, 32)
	if err != nil || sf == 0 {
		return
	}

	if sf < mf {
		ratio := math.Floor(sf * 100 / mf)
		return fmt.Sprint(ratio)
	}

	return
}

func parsePage(site string, page []byte) (*searchResult, error) {
	p := parserTable[site]
	doc, err := getDoc(page, p.encoding)
	if err != nil {
		return nil, err
	}

	items := doc.Find(p.listSelector)
	n := items.Length()
	books := make([]map[string]interface{}, n)

	items.Each(func(i int, item *goquery.Selection) {
		dict := p.itemParseFunc(i, item)
		for k, v := range dict {
			switch s := v.(type) {
			case string:
				if len(s) > 0 {
					dict[k] = strings.TrimSpace(s)
				}
			}
		}

		books[i] = dict
	})

	var ret *searchResult
	if p.getPrevNextUrl != nil {
		totalPages, prevPageUrl, nextPageUrl := p.getPrevNextUrl(doc)
		ret = &searchResult{
			books:       books,
			prevPageUrl: prevPageUrl,
			nextPageUrl: nextPageUrl,
			totalPages:  totalPages,
		}
	} else {
		// for parse site bestseller
		ret = &searchResult{
			books: books,
		}
	}

	return ret, nil
}

func parseDangDangItem(idx int, r *goquery.Selection) map[string]interface{} {
	var name, author, press, pub_date string
	var store_price, market_price, discount string
	var book_img, book_url, describe string
	var reviews, review_url string
	var allstars, halfstars, nonestars string
	var author_links string

	name = getFirstText(r, "li.maintitle a")

	publisherInfo := r.Find("li.publisher_info")
	m := dangdangPubRe.FindStringSubmatch(publisherInfo.Text())
	if m != nil {
		author, pub_date, press = m[1], m[2], m[3]
	}

	authors := publisherInfo.Find("a")
	n := authors.Length()
	authors.Each(func(idx int, s *goquery.Selection) {
		if idx != n-1 {
			// last link is press, skip it
			author_links += "\t" + s.Text()
		}
	})

	priceDiv := r.Find("div.price")
	store_price = getFirstText(priceDiv, "span.price_d")
	market_price = getFirstText(priceDiv, "span.price_m")
	if m := digitRe.FindString(getFirstText(priceDiv, "span.discount")); len(m) != 0 {
		discount = m
	}

	picInfo := r.Find("div.pic a")
	if picInfo.Length() > 0 {
		book_img, _ = picInfo.Find("img").Attr("src")
		book_url, _ = picInfo.Attr("href")
	}

	star_info := r.Find("li.starlevel")
	review_info := star_info.Find("span a")

	allstars = fmt.Sprint(star_info.Find(`img[src$="star_all.png"]`).Length())
	halfstars = fmt.Sprint(star_info.Find(`img[src$="star_half.png"]`).Length())
	nonestars = fmt.Sprint(star_info.Find(`img[src$="star_none.png"]`).Length())

	reviews = review_info.Text()
	review_url, _ = review_info.Attr("href")

	describe = getFirstText(r, "li.describ")

	dict := map[string]interface{}{
		"name":     name,
		"author":   author,
		"press":    press,
		"pub_date": pub_date,

		"store_price":  store_price,
		"market_price": market_price,
		"discount":     discount,

		"book_img": book_img,
		"book_url": book_url,

		"reviews":    reviews,
		"review_url": review_url,

		"allstars":  allstars,
		"halfstars": halfstars,
		"nonestars": nonestars,

		"describe":     describe,
		"author_links": author_links,
	}

	return dict
}

func parse360buyItem(idx int, r *goquery.Selection) map[string]interface{} {
	var name, author, press, pub_date string
	var store_price, market_price, discount string
	var book_img, book_url string
	var reviews, review_url string
	var allstars, nonestars string
	var author_links string
	var n int

	pname := r.Find("dt.p-name a")
	name = pname.Text()
	book_url, _ = pname.Attr("href")

	pressInfo := r.Find("div.p-info")
	if pressInfo.Length() > 0 {
		pressInfo = pressInfo.First()
	}
	pressInfoContents := pressInfo.Contents()
	n = pressInfoContents.Length()
	canStopAddText := false
	pressInfoContents.Each(func(idx int, s *goquery.Selection) {
		if idx == 0 {
			return
		}

		node := pressInfoContents.Get(idx)
		if node.DataAtom == atom.Br {
			canStopAddText = true
		} else {
			if !canStopAddText {
				author += s.Text()
			}
		}
	})

	pressInfoA := pressInfo.Find("a")
	n = pressInfoA.Length()
	pressInfoA.Each(func(idx int, s *goquery.Selection) {
		txt := s.Text()
		if idx == n-1 {
			press = txt
		} else {
			if author_links != "" {
				author_links += "\t"
			}
			author_links += s.Text()
		}
	})

	pgrade := r.Find("div.p-grade")
	pub_date = jingdongPubdateRe.FindString(getFirstText(pgrade, "span.date-pub"))
	pReview := pgrade.Find("a")
	if m := digitRe.FindString(pReview.Text()); len(m) != 0 {
		reviews = m
	}
	review_url, _ = pReview.Attr("href")

	s := pgrade.Find("div.star")
	for i, sa := range []string{"sa5", "sa4", "sa3", "sa2", "sa1", "sa0"} {
		if s.HasClass(sa) {
			allstars = fmt.Sprint(5 - i)
			nonestars = fmt.Sprint(i)
			break
		}
	}

	store_price, _ = r.Find("div.p-info strong img").Attr("data-lazyload")
	if len(store_price) == 0 {
		store_price, _ = r.Find("div.p-info strong img").Attr("src")
	}
	market_price = getFirstText(r, "div.mark-price del")

	book_img, _ = r.Find("div.bookimg a img").Attr("data-lazyload")

	stars := fmt.Sprint(r.Find(`li.starlevel img[src$="star_all.png"]`).Length())
	stars += stars

	if m := digitRe.FindString(getFirstText(r, `div.p-info span[id^="book_discount"]`)); len(m) != 0 {
		discount = m
	}

	dict := map[string]interface{}{
		"name":         name,
		"author":       author,
		"press":        press,
		"pub_date":     pub_date,
		"store_price":  store_price,
		"market_price": market_price,
		"book_img":     book_img,
		"book_url":     book_url,
		"discount":     discount,

		"reviews":    reviews,
		"review_url": review_url,

		"allstars":  allstars,
		"nonestars": nonestars,
		"halfstars": "0",

		"author_links": author_links,
	}

	return dict
}

func parseAmazonItem(idx int, r *goquery.Selection) map[string]interface{} {
	if r.Find("ul.rsltL").Length() > 0 {
		return parseAmazonItemB(idx, r)
	}

	return parseAmazonItemA(idx, r)
}

func parseAmazonItemA(idx int, r *goquery.Selection) map[string]interface{} {
	var name, author, press, pub_date string
	var store_price, market_price, discount string
	var book_img, book_url string
	var reviews, review_url string
	var stars, stars_url string
	var n int

	ptitle := r.Find("div.data h3.title")

	pbook := ptitle.Find("a")
	if pbook.Length() > 0 {
		f := pbook.First()
		name = f.Text()
		book_url, _ = f.Attr("href")
	}

	ptitle.Find("span").Each(func(i int, s *goquery.Selection) {
		t := s.Text()
		if i == 0 {
			author = t
		} else if i == 1 {
			press = t
		} else if i == 2 {
			if m := dateRe.FindString(t); len(m) > 0 {
				pub_date = m
			}
		}
	})

	pressInfo := r.Find("div.p-info")

	n = pressInfo.Length()
	pressInfo.Each(func(i int, s *goquery.Selection) {
		t := s.Text()
		if i == n-1 {
			press = t
		} else {
			author += t + " "
		}
	})

	stars_url, _ = r.Find("div.stars div.asinReviewsSummary a img").Attr("src")

	reviewA := r.Find("div.reviewsCount a")
	if m := digitRe.FindString(reviewA.Text()); len(m) != 0 {
		reviews = m
	}
	review_url, _ = reviewA.Attr("href")
	stars, _ = reviewA.Attr("alt")

	priceDiv := r.Find("div.newPrice")
	if m := priceRe.FindString(getFirstText(priceDiv, "strike")); len(m) > 0 {
		market_price = m
	}
	if m := priceRe.FindString(getFirstText(priceDiv, "span.price")); len(m) > 0 {
		store_price = m
	}

	// No amazon office product, try third party
	if store_price == "" {
		store_price = priceRe.FindString(getFirstText(r, "div.usedNewPrice span.price"))
	}

	discount = calcDiscount(market_price, store_price)

	book_img, _ = r.Find("div.image a img").Attr("src")

	dict := map[string]interface{}{
		"name":         name,
		"author":       author,
		"press":        press,
		"pub_date":     pub_date,
		"store_price":  store_price,
		"market_price": market_price,
		"book_img":     book_img,
		"book_url":     book_url,
		"discount":     discount,

		"reviews":    reviews,
		"review_url": review_url,

		"stars_url": stars_url,
		"stars":     stars,
	}

	return dict
}

func parseAmazonItemB(idx int, r *goquery.Selection) map[string]interface{} {
	var name, author, press, pub_date string
	var store_price, market_price, discount string
	var book_img, book_url string
	var reviews, review_url string
	var stars, stars_url string

	ptitle := r.Find("h3.newaps")

	pbook := ptitle.Find("a")
	if pbook.Length() > 0 {
		f := pbook.First()
		name = f.Text()
		book_url, _ = f.Attr("href")
	}

	book_img, _ = r.Find("div.image a img").Attr("src")

	publisherInfo := getFirstText(ptitle, "span.med")
	if m := amazon_B_publisherRe.FindStringSubmatch(publisherInfo); m != nil {
		author = m[1]
		press = m[2]
		pub_date = m[3]
	}

	leftPanel := r.Find("ul.rsltL")
	if leftPanel.Length() > 0 {
		price_m := leftPanel.Find("del")
		if price_m.Length() > 0 {
			market_price = priceRe.FindString(price_m.Text())
			price_s := price_m.Next()
			store_price = priceRe.FindString(price_s.Text())
		}

		discount = calcDiscount(market_price, store_price)

		// No amazon office product, try third party
		if store_price == "" {
			store_price = priceRe.FindString(getFirstText(leftPanel, "span.price"))
		}
	}

	rightPanel := r.Find("ul.rsltR")
	if rightPanel.Length() > 0 {
		star_img := rightPanel.Find("div.asinReviewsSummary a img")
		stars_url, _ = star_img.Attr("src")
		stars, _ = star_img.Attr("alt")

		reviewA := rightPanel.Find("span.rvwCnt a")
		review_url, _ = reviewA.Attr("href")
		if m := digitRe.FindString(reviewA.Text()); m != "" {
			reviews = m
		}
	}

	dict := map[string]interface{}{
		"name":         name,
		"author":       author,
		"press":        press,
		"pub_date":     pub_date,
		"store_price":  store_price,
		"market_price": market_price,
		"book_img":     book_img,
		"book_url":     book_url,
		"discount":     discount,

		"reviews":    reviews,
		"review_url": review_url,

		"stars_url": stars_url,
		"stars":     stars,
	}

	return dict
}

func getPrevNextUrlDangDang(doc *goquery.Document) (totalPages int, prevPageUrl string, nextPageUrl string) {
	paginDiv := doc.Find("div.paginating")
	if paginDiv.Length() == 0 {
		return
	}

	if m := dangdangTotalPagesRe.FindStringSubmatch(getFirstText(paginDiv, "div.pageform")); m != nil {
		totalPages, _ = strconv.Atoi(m[1])
	}

	paginDiv.Find("a.pagebtn").Each(func(_ int, s *goquery.Selection) {
		if !s.HasClass("disabled") {
			name, _ := s.Attr("name")
			if name == "link_page_next" {
				nextPageUrl, _ = s.Attr("href")
			} else {
				prevPageUrl, _ = s.Attr("href")
			}
		}
	})

	return
}

func getPrevNextUrl360buy(doc *goquery.Document) (totalPages int, prevPageUrl string, nextPageUrl string) {
	paginDiv := doc.Find("div.pagin")
	if paginDiv.Length() == 0 {
		return
	}

	if s := jingdongTotalPagesRe.FindStringSubmatch(getFirstText(paginDiv, "span.text")); s != nil {
		totalPages, _ = strconv.Atoi(s[1])
	}

	prevA := paginDiv.Find("a.prev")
	prevPageUrl, _ = prevA.Attr("href")

	nextA := paginDiv.Find("a.next")
	nextPageUrl, _ = nextA.Attr("href")

	return
}

func getPrevNextUrlAmazon(doc *goquery.Document) (totalPages int, prevPageUrl string, nextPageUrl string) {
	if s := doc.Find("#pagnPrevLink"); s.Length() > 0 {
		prevPageUrl, _ = s.Attr("href")
	}

	if prevPageUrl == "" {
		if s := doc.Find("#pagnPrevString"); s.Length() > 0 {
			prevPageUrl, _ = s.First().Parent().Attr("href")
		}
	}

	if s := doc.Find("#pagnNextLink"); s.Length() > 0 {
		nextPageUrl, _ = s.Attr("href")
	}

	if nextPageUrl == "" {
		if s := doc.Find("#pagnNextString"); s.Length() > 0 {
			nextPageUrl, _ = s.First().Parent().Attr("href")
		}
	}

	s := doc.Find("span.pagnDisabled").Text()
	totalPages, _ = strconv.Atoi(s)
	return
}

func parseDangDangBS(idx int, r *goquery.Selection) map[string]interface{} {
	var name, author, press, pub_date string
	var store_price, market_price, discount string
	var book_img, book_url string
	var reviews, review_url string

	bookInfoDiv := r.Find("div.book_info")
	if bookInfoDiv.Length() > 0 {
		bookNameA := bookInfoDiv.Find("li.bookname a")
		name = bookNameA.Text()
		book_url, _ = bookNameA.Attr("href")
		if m := dangdangBSAuthorRe.FindStringSubmatch(getFirstText(bookInfoDiv, "li.author")); m != nil {
			author = m[1]
		}
		if m := dangdangBSPressRe.FindStringSubmatch(getFirstText(bookInfoDiv, "li.press")); m != nil {
			press = m[1]
		}
		pub_date = jingdongPubdateRe.FindString(getFirstText(bookInfoDiv, "li.press_date"))
	}

	arrowDiv := r.Find("div.arrow")
	if arrowDiv.Length() > 0 {
		reviewA := arrowDiv.Find("span.writerno a")
		reviews = reviewA.Text()
		review_url, _ = reviewA.Attr("href")
	}

	priceDiv := r.Find("li.price")
	market_price = priceRe.FindString(getFirstText(priceDiv, "span.price_pre"))
	store_price = priceRe.FindString(getFirstText(priceDiv, "span.price_new"))

	discount = digitRe.FindString(getFirstText(arrowDiv, "li.discount"))

	picInfo := r.Find("span.book_pic a")
	if picInfo.Length() > 0 {
		book_img, _ = picInfo.Find("img").Attr("src")
		if book_url == "" {
			book_url, _ = picInfo.Attr("href")
		}
	}

	dict := map[string]interface{}{
		"name":     name,
		"author":   author,
		"press":    press,
		"pub_date": pub_date,

		"store_price":  store_price,
		"market_price": market_price,
		"discount":     discount,

		"book_img": book_img,
		"book_url": book_url,

		"reviews":    reviews,
		"review_url": review_url,
		"site":       "dangdang",
		"rank":       fmt.Sprint(idx + 1),
	}

	return dict
}

func parse360buyBS(idx int, r *goquery.Selection) map[string]interface{} {
	var name, author, press string
	var store_price, market_price, discount string
	var book_img, book_url string

	bookNameA := r.Find("dt.p-name a")

	name = bookNameA.Text()
	book_url, _ = bookNameA.Attr("href")

	var pubInfoText string
	var priceInfo *goquery.Selection
	r.Find("div.p-info").Each(func(idx int, s *goquery.Selection) {
		if idx == 0 {
			pubInfoText = s.Text()
		} else if idx == 1 {
			priceInfo = s
		}
	})

	if m := jingdongBSPinfoRe.FindStringSubmatch(pubInfoText); m != nil {
		author = m[1]
		press = m[2]
	}

	market_price = priceRe.FindString(getFirstText(priceInfo, "del"))
	store_price = priceRe.FindString(getFirstText(priceInfo, "span"))

	picInfo := r.Find("div.p-img a")
	if picInfo.Length() > 0 {
		book_img, _ = picInfo.Find("img").Attr("src")
		if book_url == "" {
			book_url, _ = picInfo.Attr("href")
		}
	}

	discount = calcDiscount(market_price, store_price)

	dict := map[string]interface{}{
		"name":   name,
		"author": author,
		"press":  press,

		"store_price":  store_price,
		"market_price": market_price,
		"discount":     discount,

		"book_img": book_img,
		"book_url": book_url,
		"site":     "360buy",
		"rank":     fmt.Sprint(idx + 1),
	}

	return dict
}

func parseAmazonBS(idx int, r *goquery.Selection) map[string]interface{} {
	var name, book_img, book_url string

	bookImageA := r.Find("div.zg_image a")
	book_img, _ = bookImageA.Find("img").Attr("src")
	book_url, _ = bookImageA.Attr("href")

	name = getFirstText(r, "div.zg_title a")

	dict := map[string]interface{}{
		"name":     name,
		"book_img": book_img,
		"book_url": book_url,
		"rank":     fmt.Sprint(idx + 1),
	}

	return dict
}
