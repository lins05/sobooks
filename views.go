package main

import (
	"fmt"
	"log"
	"net/http"
	"strconv"
)

var _ = fmt.Println

func testHandler(w http.ResponseWriter, r *http.Request) {
	err := T("test.html").Execute(w, nil)
	if err != nil {
		log.Println(err)
	}
}

func feedbackHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		err := T("feedback.html").Execute(w, nil)
		if err != nil {
			log.Println(err)
		}
		return
	}

	// POST
	email := r.FormValue("email")
	suggestion := r.FormValue("suggestion")

	// "email" can be empty, but "suggestion" can't.
	if suggestion == "" {
		return
	}

	recordFeedback(email, suggestion)
}

func listFeedbackHandler(w http.ResponseWriter, r *http.Request) {
	fbs, err := getFeedback()
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}

	err = T("list_feedback.html").Execute(w, fbs)
	if err != nil {
		log.Println(err)
	}
}

func homePageHanlder(w http.ResponseWriter, r *http.Request) {
	err := T("home.html").Execute(w, nil)
	if err != nil {
		log.Println(err)
	}
}

func searchPageHandler(w http.ResponseWriter, r *http.Request) {
	var err error
	keyword := r.FormValue("keyword")
	if len(keyword) == 0 {
		http.Redirect(w, r, SETTINGS["SITE_ROOT"], http.StatusSeeOther)
		return
	}

	sort := r.FormValue("sort")
	if len(sort) == 0 {
		sort = "default"
	}

	history := updateSearchHistory(w, r, keyword)

	dict := map[string]interface{}{
		"keyword": keyword,
		"sort":    sort,
		"history": history,
	}

	err = T("search.html").Execute(w, dict)
	if err != nil {
		log.Println(err)
	}
}

func ajaxSearchHandler(w http.ResponseWriter, r *http.Request) {
	var site, keyword string
	var url string
	var sort string
	var pageNumber int
	var totalPages string

	site = r.FormValue("site")
	if len(site) == 0 {
		http.Error(w, "invalid site", http.StatusBadRequest)
		return
	}
	if site != "360buy" && site == "dangdang" && site == "amazon" {
		http.Error(w, "unknown site", http.StatusBadRequest)
		return
	}

	keyword = r.FormValue("keyword")
	if len(keyword) == 0 {
		http.Error(w, "invalid keyword", http.StatusBadRequest)
		return
	}

	sort = r.FormValue("sort")
	if len(sort) == 0 {
		sort = "default"
	} else if sort != "default" && !isValidSort(site, sort) {
		http.Error(w, "invalid sort", http.StatusBadRequest)
		return
	}

	pageStr := r.FormValue("page")
	if len(pageStr) == 0 {
		pageNumber = 1
	} else {
		var err error
		pageNumber, err = strconv.Atoi(pageStr)
		if err != nil || pageNumber < 1 || pageNumber > 100 {
			http.Error(w, "invalid page", http.StatusBadRequest)
			return
		}
	}

	url = r.FormValue("url")
	totalPages = r.FormValue("totalPages")

	ret, err := getBookInfo(site, url, keyword, sort)
	if err != nil {
		log.Printf("error when search %s: %s\n", site, err)
		http.Error(w, "", http.StatusInternalServerError)
		return
	}

	for _, book := range ret.books {
		if site == "dangdang" || site == "360buy" {
			book["allstars"] = convertStars(book["allstars"])
			book["halfstars"] = convertStars(book["halfstars"])
			book["nonestars"] = convertStars(book["nonestars"])

			// add links to author
			book["authors"] = convertAuthorLinks(book["author"], book["author_links"])
		} else {
			book["authors"] = convertAuthorLinksAmazon(book["author"])
		}

		maxDesc := 300
		if site == "dangdang" {
			book["describe"] = trim(book["describe"], maxDesc)
		}
	}

	// For amazon A/B testing: Sometimes total pages number would not be
	// present, so we just pass it on from request to request
	var totalPageNumber int
	if ret.totalPages > 0 {
		totalPageNumber = ret.totalPages
	} else {
		totalPageNumber, _ = strconv.Atoi(totalPages)
	}

	var hasPaginator = true
	if pageNumber == 1 && len(ret.books) == 0 {
		hasPaginator = false
	}

	dict := map[string]interface{}{
		"hasPaginator":    hasPaginator,
		"pageNumber":      pageNumber,
		"totalPageNumber": totalPageNumber,
		"site":            site,
		"keyword":         keyword,
		"books":           ret.books,
		"prevPageUrl":     ret.prevPageUrl,
		"nextPageUrl":     ret.nextPageUrl,
	}

	err = T0("results.html").Execute(w, dict)
	if err != nil {
		log.Println(err)
	}
}

func bestSellerHandler(w http.ResponseWriter, r *http.Request) {
	kind := r.FormValue("kind")
	if kind != "" {
		if kind != "daily" && kind != "weekly" {
			kind = ""
		}
	}

	var toKey = func(site string) string {
		// Amazon does not distinguish "daily", "weekly", or "monthly"
		if site == "amazon" {
			return "amazon"
		}

		if kind != "" {
			return site + "-" + kind
		}

		return site
	}

	var err error
	dangdangRet, err := getBestSeller(toKey("dangdang"))
	if err != nil {
		log.Println("[bestSellerHandler] error when get best seller dangdang:", err)
	}
	jingdongRet, err := getBestSeller(toKey("360buy"))
	if err != nil {
		log.Println("[bestSellerHandler] error when get best seller 360buy:", err)
	}
	amazonRet, err := getBestSeller(toKey("amazon"))
	if err != nil {
		log.Println("[bestSellerHandler] error when get best seller amazon:", err)
	}

	dict := map[string]interface{}{
		"kind":     kind,
		"dangdang": dangdangRet,
		"jingdong": jingdongRet,
		"amazon":   amazonRet,
	}

	err = T("best_seller.html").Execute(w, dict)
	if err != nil {
		log.Println(err)
	}
}
