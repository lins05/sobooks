package main

import (
	"encoding/json"
	"fmt"
	"log"
	"strings"
	"time"
)

var _ = fmt.Println

var BESTSELLER_UPDATE_INTERVAL_MINUTE = 60 // Update every 60 Minutes

// Default: monthly
// Other: weekly, daily
var bestSellerUrlMap = map[string]string{
	"dangdang":        "http://bang.dangdang.com/book/bestSeller/?type=recent30&catpath=01.00.00.00.00.00&catname=%CD%BC%CA%E9&show=T&pc=20",
	"dangdang-weekly": "http://bang.dangdang.com/book/bestSeller/?type=recent7&catpath=01.00.00.00.00.00&catname=%CD%BC%CA%E9&show=T&pc=20",
	"dangdang-daily":  "http://bang.dangdang.com/book/bestSeller/?type=24hours&catpath=01.00.00.00.00.00&catname=%CD%BC%CA%E9&show=T&pc=20",

	"360buy":        "http://www.360buy.com/booktop-3-0-1.html",
	"360buy-weekly": "http://www.360buy.com/booktop-2-0-1.html",
	"360buy-daily":  "http://www.360buy.com/booktop-1-0-1.html",

	"amazon": "http://www.amazon.cn/gp/bestsellers/books/",
}

func init() {
	go bestSellerWorker()
}

func bestSellerWorker() {
	interval := time.Minute * time.Duration(BESTSELLER_UPDATE_INTERVAL_MINUTE)
	for {
		log.Println("start to fetch best seller")
		for site, _ := range bestSellerUrlMap {
			go fetchBestSeller(site)
		}
		<-time.After(interval)
	}
}

// Fetch bestseller page; parse results; save in database;
func fetchBestSeller(site string) {
	u := bestSellerUrlMap[site]
	page, err := getPage(u, 3)
	if err != nil {
		log.Println("error when get page:", err)
		return
	}

	results, err := parsePage(stripBSSuffix(site)+"-bs", page)
	if err != nil {
		log.Println("error when parse page:", err)
		return
	}

	if len(results.books) == 0 {
		log.Printf("error when parse bestseller of %s: zero results\n", site)
		return
	}

	buf, err := json.Marshal(results.books)
	if err != nil {
		log.Println("error when results to json:", err)
		return
	}

	storeBestSeller(site, buf)
}

func getBestSeller(site string) ([]map[string]interface{}, error) {
	content, err := readBestSeller(site)
	if err != nil {
		return nil, err
	}

	if content == nil {
		return nil, nil
	}

	var ret []map[string]interface{}
	err = json.Unmarshal(content, &ret)
	if err != nil {
		return nil, err
	}

	return ret, nil
}

func stripBSSuffix(s string) string {
	pos := strings.Index(s, "-")
	if pos > 0 {
		return s[:pos]
	}

	return s
}
